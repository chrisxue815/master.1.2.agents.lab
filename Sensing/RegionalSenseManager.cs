﻿using System;
using System.Collections.Generic;
using CXToolbox.Utilities;
using FiniteStateMachine.Components;
using Microsoft.Xna.Framework;

namespace FiniteStateMachine.Sensing
{
    public class RegionalSenseManager : Component
    {
        private readonly List<Sensor> sensors;
        private readonly PriorityLinkedQueue<Notification> notificationQueue;

        public RegionalSenseManager()
        {
            sensors = new List<Sensor>();
            notificationQueue = new PriorityLinkedQueue<Notification>();
        }

        public void AddSensor(Sensor sensor)
        {
            sensors.Add(sensor);
        }

        public void AddSignal(Signal signal)
        {
            foreach (var sensor in sensors)
            {
                if (!sensor.DetectModality(signal.Modality)) continue;

                var distance = Vector2.Distance(signal.Position, sensor.Position);

                if (signal.Modality.MaxRange < distance) continue;

                var intensity = signal.Strength * (float)Math.Pow(signal.Modality.Attenuation, distance);

                if (intensity < sensor.Threshold) continue;

                if (!signal.Modality.ExtraCheck(signal, sensor)) continue;

                var time = Game.TotalSeconds + distance * signal.Modality.InverseTransmissionSpeed;

                var notification = new Notification
                {
                    Time = time,
                    Sensor = sensor,
                    Signal = signal
                };

                notificationQueue.Enqueue(notification);
            }
        }

        public override void Update()
        {
            var currentTime = Game.TotalSeconds;

            while (notificationQueue.Count > 0)
            {
                var notification = notificationQueue.Peek();

                if (notification.Time < currentTime)
                {
                    notification.Sensor.Notify(notification.Signal);
                    notificationQueue.Dequeue();
                }
                else
                {
                    break;
                }
            }
        }
    }
}
