﻿using CXToolbox.Utilities;
using FiniteStateMachine.Sensing.Modalities;

namespace FiniteStateMachine.Sensing
{
    public class Signal
    {
        public Modality Modality;
        public Vector2I Position;
        public float Strength;

        public Signal(Modality modality, ref Vector2I position, float strength = 1)
        {
            Modality = modality;
            Position = position;
            Strength = strength;
        }
    }
}
