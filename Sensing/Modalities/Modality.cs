﻿namespace FiniteStateMachine.Sensing.Modalities
{
    public abstract class Modality
    {
        public float MaxRange;
        public float Attenuation;
        public float InverseTransmissionSpeed;

        public abstract bool ExtraCheck(Signal signal, Sensor sensor);
    }
}
