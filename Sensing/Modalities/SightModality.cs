﻿using System;
using CXToolbox.Utilities;
using Microsoft.Xna.Framework;

namespace FiniteStateMachine.Sensing.Modalities
{
    public class SightModality : Modality
    {
        /// <summary>
        /// In radians. Omnidirectional.
        /// </summary>
        public const float HalfFieldOfView = MathHelper.Pi;

        public SightModality(float maxRange, float attenuation = 1, float inverseTransmissionSpeed = 0)
        {
            MaxRange = maxRange;
            Attenuation = attenuation;
            InverseTransmissionSpeed = inverseTransmissionSpeed;
        }

        public override bool ExtraCheck(Signal signal, Sensor sensor)
        {
            var sensorPosition = sensor.Position;
            return CheckSightCone(ref signal.Position, ref sensorPosition, sensor.Orientation)
                   && CheckLineOfSight(ref signal.Position, ref sensorPosition);
        }

        public bool CheckSightCone(ref Vector2I signalPosition, ref Vector2I sensorPosition, float sensorOrientation)
        {
            var toSignal = signalPosition - sensorPosition;
            if (toSignal.IsZero()) return true;

            var dot = Vector2.Dot(toSignal, Vector2.UnitX);
            var angle = Math.Acos(dot/toSignal.Length());
            return angle <= HalfFieldOfView;
        }

        public bool CheckLineOfSight(ref Vector2I signalPosition, ref Vector2I sensorPosition)
        {
            return Game1.Instance.EnvironmentManager.CheckLineOfSight(signalPosition, sensorPosition);
        }
    }
}
