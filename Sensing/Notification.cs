﻿using System;

namespace FiniteStateMachine.Sensing
{
    public class Notification : IComparable<Notification>, IEquatable<Notification>
    {
        public float Time;
        public Sensor Sensor;
        public Signal Signal;

        public int CompareTo(Notification other)
        {
            var diff = Time - other.Time;
            return diff < 0 ? -1 : (diff > 0 ? 1 : 0);
        }

        public bool Equals(Notification other)
        {
            return Time.Equals(other.Time) && Sensor.Equals(other.Sensor) && Signal.Equals(other.Signal);
        }
    }
}
