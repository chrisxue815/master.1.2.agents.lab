﻿using CXToolbox.Utilities;
using FiniteStateMachine.Sensing.Modalities;

namespace FiniteStateMachine.Sensing
{
    public interface ISensorEntity
    {
        Vector2I CurrentPosition { get; }
        float Orientation { get; }

        void HandleSignal(Signal signal);
        bool DetectModality(Modality modality);
    }

    public class Sensor
    {
        public ISensorEntity SensorEntity;
        public float Threshold;

        public Vector2I Position { get { return SensorEntity.CurrentPosition; } }
        public float Orientation { get { return SensorEntity.Orientation; } }

        public Sensor(ISensorEntity sensorEntity, float threshold = 1)
        {
            SensorEntity = sensorEntity;
            Threshold = threshold;
        }

        public void Notify(Signal signal)
        {
            SensorEntity.HandleSignal(signal);
        }

        public bool DetectModality(Modality modality)
        {
            return SensorEntity.DetectModality(modality);
        }
    }
}
