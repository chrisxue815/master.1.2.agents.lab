﻿using System;
using System.Collections.Generic;
using System.IO;
using CXToolbox.PathFinding.AStar;
using CXToolbox.Tiled;
using CXToolbox.Utilities;
using FiniteStateMachine.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FiniteStateMachine.Components
{
    public enum LayerIndex { Ground, Terrain, Entities1, Entities2, Locations }

    public class EnvironmentManager : Component
    {
        private TmxMap map;

        private readonly Dictionary<Location, List<Vector2I>> locations;

        private AStarToolbox astar;

        private readonly List<Texture2D> textures;
        private Texture2D groundTexture;

        public EnvironmentManager()
        {
            locations = new Dictionary<Location, List<Vector2I>>();
            textures = new List<Texture2D>();
        }

        public bool IsPassable(ref Vector2I position)
        {
            var passageLayers = new[] { LayerIndex.Terrain, LayerIndex.Entities1 };

            foreach (var passageLayer in passageLayers)
            {
                var passageTiles = map.Layers[(int)passageLayer].Tiles;
                var tile = passageTiles.GetValueOrNull(position.X, position.Y);
                if (tile == null || tile.Gid != 0) return false;
            }

            foreach (var agent in AgentManager.All)
            {
                if (agent.CurrentPosition.Equals(position)) return false;
            }

            return true;
        }

        public bool CheckLineOfSight(Vector2I a, Vector2I b)
        {
            if (a.Equals(b)) return true;

            // Bresenham
            var steep = Math.Abs(b.Y - a.Y) > Math.Abs(b.X - a.X);
            if (steep)
            {
                Utils.Swap(ref a.X, ref a.Y);
                Utils.Swap(ref b.X, ref b.Y);
            }
            if (a.X > b.X)
            {
                Utils.Swap(ref a, ref b);
            }
            var deltax = b.X - a.X;
            var deltay = Math.Abs(b.Y - a.Y);
            var error = deltax / 2;
            var y = a.Y;
            var ystep = a.Y < b.Y ? 1 : -1;
            for (var x = a.X + 1; x < b.X; x++)
            {
                var pos = steep ? new Vector2I(y, x) : new Vector2I(x, y);
                if (!IsPassable(ref pos)) return false;
                error -= deltay;
                if (error < 0)
                {
                    y += ystep;
                    error += deltax;
                }
            }
            return true;
        }

        public List<Vector2I> FindPath(ref Vector2I start, ref Vector2I goal)
        {
            return astar.FindPath(ref start, ref goal);
        }

        public Location? GetLocation(ref Vector2I position)
        {
            var locationTiles = map.Layers[(int)LayerIndex.Locations].Tiles;
            var tile = locationTiles.GetValueOrNull(ref position);
            var gid = tile.Gid;
            if (gid >= 1 && gid <= LocationExt.Count)
            {
                return (Location)(gid - 1);
            }
            return null;
        }

        public void GetPosition(Location location, out Vector2I position)
        {
            var positions = locations[location];
            var rand = RandomExt.NextInt(positions.Count);
            position = positions[rand];
        }

        public bool GetPassablePosition(Location location, out Vector2I position)
        {
            var positions = locations[location];
            var rand = RandomExt.NextInt(positions.Count);
            position = positions[rand];
            if (IsPassable(ref position))
            {
                return true;
            }

            var indices = new List<int>(positions.Count - 1);
            for (var i = 0; i < positions.Count; i++)
            {
                if (i != rand)
                {
                    indices.Add(i);
                }
            }

            while (indices.Count > 0)
            {
                rand = RandomExt.NextInt(indices.Count);
                position = positions[indices[rand]];
                if (IsPassable(ref position))
                {
                    return true;
                }
                indices.RemoveAt(rand);
            }

            position = positions[0];
            return false;
        }

        public void ToScreenPosition(ref Vector2I position, out Vector2 screenPosition)
        {
            screenPosition = new Vector2(map.TileWidth * position.X, map.TileHeight * position.Y);
        }

        public override void LoadContent()
        {
            using (var stream = TitleContainer.OpenStream("Content/Level.tmx"))
            {
                var reader = new StreamReader(stream);
                map = TmxMap.Open(reader);
            }

            var locationTiles = map.Layers[(int)LayerIndex.Locations].Tiles;

            foreach (var locationTile in locationTiles)
            {
                var gid = locationTile.Gid;
                if (gid >= 1 && gid <= LocationExt.Count)
                {
                    var location = (Location)(gid - 1);
                    List<Vector2I> positions;
                    locations.TryGetValue(location, out positions);
                    if (positions == null)
                    {
                        positions = new List<Vector2I>();
                        locations[location] = positions;
                    }
                    positions.Add(new Vector2I(locationTile.X, locationTile.Y));
                }
            }

            astar = new AStarToolbox(map.Width, map.Height, IsPassable);

            //LoadTilesets();

            groundTexture = Game1.Instance.Content.Load<Texture2D>("Level");
        }

        private void LoadTilesets()
        {
            // compute tile data for drawing
            map.ComputeSourceRectangle();

            foreach (var tileset in map.Tilesets)
            {
                var contentName = tileset.ImageSource.Replace(".png", "");
                var texture = Game1.Instance.Content.Load<Texture2D>(contentName);
                textures.Add(texture);
            }
        }

        public override void Draw()
        {
            Game.Draw(groundTexture, 1);

            //DrawTiles();
        }

        private void DrawTiles()
        {
            for (var layerIndex = 0; layerIndex < map.Layers.Count; layerIndex++)
            {
                var layer = map.Layers[layerIndex];

                if (!layer.Visible) continue;

                foreach (var tile in layer.Tiles)
                {
                    if (tile.Gid == 0) continue;

                    var texture = textures[tile.TilesetIndex];
                    var tileset = map.Tilesets[tile.TilesetIndex];
                    var position = new Vector2(tile.X * tileset.TileWidth, tile.Y * tileset.TileHeight);
                    var sourceRectangle = new Rectangle(tile.TilesetX, tile.TilesetY, tileset.TileWidth,
                        tileset.TileHeight);
                    var layerDepth = 1 - 0.5f * (layerIndex - 1) - 0.01f * tile.Y;
                    Game.Draw(texture, ref position, ref sourceRectangle, layerDepth);
                }
            }
        }
    }
}
