﻿using System.Collections.Generic;
using CXToolbox.Utilities;
using FiniteStateMachine.Agents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FiniteStateMachine.Components
{
    public class Corpse
    {
        public Agent Owner;
        public Vector2I Position;
        public Vector2 ScreenPosition;
        public bool Buried;
        public bool Transformed;
        public float TransformingRate;
        public float DisappearingRate;

        public Corpse(ref Vector2I position, Agent owner)
        {
            Owner = owner;
            Position = position;
            Game1.Instance.EnvironmentManager.ToScreenPosition(ref position, out ScreenPosition);
        }
    }

    public class CorpseManager : Component
    {
        private readonly Dictionary<Vector2I, Corpse> corpses = new Dictionary<Vector2I, Corpse>();

        private Texture2D corpseTexture;
        private static readonly Rectangle CorpseRectangle = new Rectangle(64, 0, 32, 32);

        private Texture2D ghostTexture;
        private static readonly Rectangle GhostRectangle = new Rectangle(0, 0, 32, 32);

        public Corpse First
        {
            get
            {
                foreach (var corpse in corpses.Values)
                {
                    if (!corpse.Transformed) return corpse;
                }
                return null;
            }
        }

        public IEnumerable<Corpse> All
        {
            get { return corpses.Values; }
        }

        public void Add(ref Vector2I position, Agent owner)
        {
            corpses[position] = new Corpse(ref position, owner);
        }

        public void Add(ref Vector2I position, Corpse corpse)
        {
            corpse.Position = position;
            Game.EnvironmentManager.ToScreenPosition(ref position, out corpse.ScreenPosition);
            corpses[position] = corpse;
        }

        public void Remove(Corpse corpse)
        {
            corpses.Remove(corpse.Position);
        }

        public override void LoadContent()
        {
            corpseTexture = Game.Content.Load<Texture2D>("Characters/Corpse");
            ghostTexture = Game.Content.Load<Texture2D>("Characters/Ghost");
        }

        public override void Update()
        {
            var removal = new List<Corpse>();

            foreach (var corpse in corpses.Values)
            {
                if (corpse.Transformed)
                {
                    corpse.DisappearingRate += 0.01f;
                    var disappear = RandomExt.NextBool(corpse.DisappearingRate, -0.3f);
                    if (disappear) removal.Add(corpse);
                }
                else
                {
                    corpse.TransformingRate += 0.01f;
                    corpse.Transformed = RandomExt.NextBool(corpse.TransformingRate, -0.3f);
                }
            }

            foreach (var corpse in removal)
            {
                corpses.Remove(corpse.Position);
            }
        }

        public override void Draw()
        {
            foreach (var corpse in corpses.Values)
            {
                var texture = corpse.Transformed ? ghostTexture : corpseTexture;
                var rectangle = corpse.Transformed ? GhostRectangle : CorpseRectangle;
                Game.Draw(texture, ref corpse.ScreenPosition, ref rectangle, 0);
            }
        }
    }
}
