﻿namespace FiniteStateMachine.Components
{
    public abstract class Component
    {
        protected readonly Game1 Game;

        protected Component()
        {
            Game = Game1.Instance;
        }

        public virtual void Initialize()
        {
        }

        public virtual void LoadContent()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void Draw()
        {
        }
    }
}
