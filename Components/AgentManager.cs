using System.Collections.Generic;
using CXToolbox.Utilities;
using FiniteStateMachine.Agents;
using FiniteStateMachine.Models;

namespace FiniteStateMachine.Components
{
    public static class AgentManager
    {
        private static readonly Dictionary<int, Agent> Agents;

        private static int nextAgentId;

        static AgentManager()
        {
            Agents = new Dictionary<int, Agent>();
        }

        public static IEnumerable<Agent> All
        {
            get { return Agents.Values; }
        }

        public static void Add(Agent agent)
        {
            if (agent.Id != -1) return;

            agent.Id = nextAgentId++;
            Agents[agent.Id] = agent;
        }

        public static void Remove(Agent agent)
        {
            Agents.Remove(agent.Id);
        }

        public static Agent FindAgent(int id)
        {
            return Agents[id];
        }

        public static Agent FindAgent<T>()
        {
            foreach (var agent in Agents.Values)
            {
                if (agent is T)
                {
                    return agent;
                }
            }
            return null;
        }

        public static Agent FindAgent(ref Vector2I position, Agent exclusion = null)
        {
            foreach (var agent in Agents.Values)
            {
                if (agent.CurrentPosition == position && agent != exclusion)
                {
                    return agent;
                }
            }
            return null;
        }

        public static List<Agent> FindAgents(Location location, Agent exclusion = null)
        {
            var agents = new List<Agent>();
            foreach (var agent in Agents.Values)
            {
                if (agent.CurrentLocation == location && agent != exclusion)
                {
                    agents.Add(agent);
                }
            }
            return agents;
        }

        public static void Initialize()
        {
            foreach (var agent in Agents.Values)
            {
                agent.Initialize();
            }
        }

        public static void LoadContent()
        {
            foreach (var agent in Agents.Values)
            {
                agent.LoadContent();
            }
        }

        public static void Update()
        {
            foreach (var agent in Agents.Values)
            {
                agent.Update();
            }
        }

        public static void Draw()
        {
            foreach (var agent in Agents.Values)
            {
                agent.Draw();
            }
        }
    }
}
