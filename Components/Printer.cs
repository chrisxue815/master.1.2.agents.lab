using System;
using FiniteStateMachine.Agents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FiniteStateMachine.Components
{
    public static class Printer
    {
        public static int Max = 20;
        public static String[] Display = new String[Max];
        public static int[] Displaytype = new int[Max];
        public static int Top = 0;
        public static int Amount = 0;

        public static void Print(Agent agent, string message)
        {
            //var output = string.Format("{0}(#{1}): {2}", agent.Name, agent.Id, message);
            var output = string.Format("{0} {1}", agent.Id, message);
            Console.WriteLine(output);
            Console.WriteLine();
            Top++;
            if (Top >= Max) { Top = 0; }
            if (Amount < 20) { Amount++; }
            Display[Top] = output;
            Displaytype[Top] = agent.Id;

            agent.DialogueManager.Add(message);
        }

        public static void PrintMessageData(string message)
        {
            Console.WriteLine("M " + message + "\n");
            Top++;
            if (Top >= Max) { Top = 0; }
            if (Amount < 20) { Amount++; }
            Display[Top] =  message;
            Displaytype[Top] = 5;
        }

        public static void Draw(SpriteBatch spriteBatch, SpriteFont spriteFont)
        {
            spriteBatch.Begin();
            var index = Top;
            for (var i = 0; i < Amount; i++)
            {
                spriteBatch.DrawString(spriteFont, Display[index], new Vector2(10.0f, 500 - (i * 20)), Color.Black);
                index--;
                if (index < 0)
                {
                    index = Max - 1;
                }
            }
            spriteBatch.End();
        }
    }
}
