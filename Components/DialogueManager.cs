﻿using System.Collections.Generic;
using FiniteStateMachine.Agents;
using Microsoft.Xna.Framework;

namespace FiniteStateMachine.Components
{
    public class Dialogue
    {
        public string Sentence;
        public float SafeTime;

        public Dialogue(string sentence, float safeTime)
        {
            Sentence = sentence;
            SafeTime = safeTime;
        }
    }

    public class DialogueManager : Component
    {
        public const float SafeTime = 5;
        private readonly Agent agent;
        private readonly LinkedList<Dialogue> dialogues;

        public DialogueManager(Agent agent)
        {
            this.agent = agent;
            dialogues = new LinkedList<Dialogue>();
        }

        public void Add(string sentence)
        {
            while (dialogues.Count > 1)
            {
                dialogues.RemoveFirst();
            }
            RemoveOutdatedDialogues();

            if (dialogues.Count > 0 && sentence.Equals(dialogues.Last.Value.Sentence)) return;

            dialogues.AddLast(new Dialogue(sentence, Game.TotalSeconds + 2));
        }

        public void RemoveOutdatedDialogues()
        {
            var time = Game.TotalSeconds;
            for (var node = dialogues.First; node != dialogues.Last && node != null; node = node.Next)
            {
                if (node.Value.SafeTime > time) break;

                dialogues.Remove(node);
            }
        }

        public override void Update()
        {
            RemoveOutdatedDialogues();
        }

        public override void Draw()
        {
            var screenPosition = new Vector2(agent.ScreenPosition.X, agent.ScreenPosition.Y - 32);
            for (var node = dialogues.Last; node != null; node = node.Previous)
            {
                Game.SpriteBatch.DrawString(Game.SpriteFont, node.Value.Sentence, screenPosition, Color.Black);
                screenPosition.Y -= 32;
            }
        }
    }
}
