﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CXToolbox.Utilities
{
    public static class Utils
    {
        public static void Swap<T>(ref T a, ref T b)
        {
            var c = a;
            a = b;
            b = c;
        }
    }
}
