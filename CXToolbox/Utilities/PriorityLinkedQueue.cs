﻿using System;
using System.Collections.Generic;

namespace CXToolbox.Utilities
{
    public class PriorityLinkedQueue<T> where T: IComparable<T>, IEquatable<T>
    {
        private readonly LinkedList<T> list;

        public PriorityLinkedQueue()
        {
            list = new LinkedList<T>();
        }

        public int Count
        {
            get { return list.Count; }
        }

        public bool Empty
        {
            get { return list.Count == 0; }
        }

        public bool Contains(T item)
        {
            foreach (var element in list)
            {
                if (element.Equals(item)) return true;
            }
            return false;
        }

        public void Enqueue(T item)
        {
            for (var node = list.Last; ; node = node.Previous)
            {
                if (node == null)
                {
                    list.AddFirst(item);
                    break;
                }

                if (node.Value.CompareTo(item) <= 0)
                {
                    list.AddAfter(node, item);
                    break;
                }
            }
        }

        public T Dequeue()
        {
            var item = list.First.Value;
            list.RemoveFirst();
            return item;
        }

        public T Peek()
        {
            return list.First.Value;
        }
    }
}
