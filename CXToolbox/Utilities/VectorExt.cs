﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace CXToolbox.Utilities
{
    public static class VectorExt
    {
        public static IEnumerable<Vector2> FourNeighbors(this Vector2 v)
        {
            yield return new Vector2(v.X + 1, v.Y);
            yield return new Vector2(v.X, v.Y + 1);
            yield return new Vector2(v.X - 1, v.Y);
            yield return new Vector2(v.X, v.Y - 1);
        }
    }
}
