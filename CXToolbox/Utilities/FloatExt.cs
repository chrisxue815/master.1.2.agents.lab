﻿namespace CXToolbox.Utilities
{
    public static class FloatExt
    {
        public const float Epsilon = 1e-5f;
        public const float NegativeEpsilon = -Epsilon;

        public static bool IsZero(this float value)
        {
            return value > NegativeEpsilon && value < Epsilon;
        }

        public static bool Equals(this float v1, float v2)
        {
            return (v1 - v2).IsZero();
        }
    }
}
