﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace CXToolbox.Utilities
{
    public struct Vector2I
    {
        public int X;
        public int Y;

        public Vector2I(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Vector2I Zero
        {
            get { return new Vector2I(0, 0); }
        }

        public static Vector2I Left
        {
            get { return new Vector2I(-1, 0); }
        }

        public static Vector2I Right
        {
            get { return new Vector2I(1, 0); }
        }

        public static Vector2I Up
        {
            get { return new Vector2I(0, -1); }
        }

        public static Vector2I Down
        {
            get { return new Vector2I(0, 1); }
        }

        public static Vector2I RandomDirection()
        {
            switch (RandomExt.NextInt(4))
            {
                case 0:
                    return Up;
                case 1:
                    return Down;
                case 2:
                    return Left;
                case 3:
                    return Right;
                default:
                    return Zero;
            }
        }

        public IEnumerable<Vector2I> FourNeighbors()
        {
            yield return new Vector2I(X + 1, Y);
            yield return new Vector2I(X, Y + 1);
            yield return new Vector2I(X - 1, Y);
            yield return new Vector2I(X, Y - 1);
        }

        public Vector2I Reverse
        {
            get { return new Vector2I(-X, -Y); }
        }

        public float Orientation
        {
            get { return (float)Math.Acos(Dot(this, Right) / Length()); }
        }

        public bool IsAbove(Vector2I p)
        {
            return Y < p.Y;
        }

        public bool IsBelow(Vector2I p)
        {
            return Y > p.Y;
        }

        public bool IsHorizontal(Vector2I p)
        {
            return Y == p.Y;
        }

        public bool IsLeft(Vector2I p)
        {
            return X < p.X;
        }

        public bool IsRight(Vector2I p)
        {
            return X > p.X;
        }

        public bool IsVertical(Vector2I p)
        {
            return X == p.X;
        }

        public bool IsFourConnected(Vector2I p)
        {
            return X == p.X && Math.Abs(Y - p.Y) == 1
                   || Y == p.Y && Math.Abs(X - p.X) == 1;
        }

        public bool IsPositive()
        {
            return X >= 0 && Y >= 0;
        }

        public bool IsNegative()
        {
            return X <= 0 && Y <= 0;
        }

        public static implicit operator Vector2(Vector2I p)
        {
            return new Vector2(p.X, p.Y);
        }

        public static bool operator ==(Vector2I p1, Vector2I p2)
        {
            return p1.X == p2.X && p1.Y == p2.Y;
        }

        public static bool operator !=(Vector2I p1, Vector2I p2)
        {
            return !(p1 == p2);
        }

        public static Vector2I operator +(Vector2I p1, Vector2I p2)
        {
            return new Vector2I(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static Vector2I operator -(Vector2I p1, Vector2I p2)
        {
            return new Vector2I(p1.X - p2.X, p1.Y - p2.Y);
        }

        public static Vector2I operator -(Vector2I p)
        {
            return new Vector2I(-p.X, -p.Y);
        }

        public static Vector2I operator *(Vector2I p, int scale)
        {
            return new Vector2I(p.X * scale, p.Y * scale);
        }

        public static Vector2I operator *(int scale, Vector2I p)
        {
            return new Vector2I(p.X * scale, p.Y * scale);
        }

        public static Vector2 operator *(Vector2I p, float scale)
        {
            return new Vector2(p.X * scale, p.Y * scale);
        }

        public static Vector2 operator *(float scale, Vector2I p)
        {
            return new Vector2(p.X * scale, p.Y * scale);
        }

        public static int Dot(Vector2I p1, Vector2I p2)
        {
            return p1.X * p2.X + p1.Y * p2.Y;
        }

        public static int Dot(ref Vector2I p1, ref Vector2I p2)
        {
            return p1.X * p2.X + p1.Y * p2.Y;
        }

        public static Vector2I operator /(Vector2I p, int scale)
        {
            return new Vector2I(p.X / scale, p.Y / scale);
        }

        public static Vector2 operator /(Vector2I p, float scale)
        {
            return new Vector2(p.X / scale, p.Y / scale);
        }

        public float Length()
        {
            return (float)Math.Sqrt(X * X + Y * Y);
        }

        public int LengthSquared()
        {
            return X * X + Y * Y;
        }

        public static float Distance(Vector2I p1, Vector2I p2)
        {
            return (p1 - p2).Length();
        }

        public bool IsZero()
        {
            return X == 0 && Y == 0;
        }

        public bool NotZero()
        {
            return X != 0 || Y != 0;
        }

        public bool IsReverse(Vector2I p)
        {
            return X == -p.X && Y == -p.Y;
        }

        public bool IsPerpendicular(Vector2I p)
        {
            return Dot(this, p) == 0;
        }

        public bool IsNonZeroPerpendicular(Vector2I p)
        {
            return X != 0 && Y != 0 && p.X != 0 && p.Y != 0 && Dot(this, p) == 0;
        }

        public bool Equals(Vector2I other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Vector2I && Equals((Vector2I)obj);
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() + Y.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{{X:{0} Y:{1}}}", X, Y);
        }
    }
}
