﻿using System;

namespace CXToolbox.Utilities
{
    public static class ArrayExt
    {
        public static bool Contains(this Array array, Object obj)
        {
            foreach (var item in array)
            {
                if (item.Equals(obj))
                {
                    return true;
                }
            }

            return false;
        }

        public static T GetValueOrNull<T>(this T[] array, int index) where T : class
        {
            if (index >= 0 && index < array.Length)
            {
                return array[index];
            }
            return null;
        }

        public static T GetValueOrNull<T>(this T[,] array, int x, int y) where T : class
        {
            if (x >= 0 && x < array.GetLength(0) && y >= 0 && y < array.GetLength(1))
            {
                return array[x, y];
            }
            return null;
        }

        public static T GetValueOrNull<T>(this T[,] array, ref Vector2I index) where T : class
        {
            var x = index.X;
            var y = index.Y;
            if (x >= 0 && x < array.GetLength(0) && y >= 0 && y < array.GetLength(1))
            {
                return array[x, y];
            }
            return null;
        }
    }
}
