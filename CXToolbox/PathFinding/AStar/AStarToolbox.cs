﻿using System;
using System.Collections.Generic;
using CXToolbox.Utilities;

namespace CXToolbox.PathFinding.AStar
{
    public class AStarToolbox
    {
        public int Width;
        public int Height;

        public delegate bool IsPassableDelegate(ref Vector2I position);
        public IsPassableDelegate IsPassable;

        public AStarToolbox(int width, int height, IsPassableDelegate isPassable)
        {
            Width = width;
            Height = height;
            IsPassable = isPassable;
        }

        /// <summary>
        /// 4-neighbor
        /// </summary>
        /// <param name="start"></param>
        /// <param name="goal"></param>
        /// <returns></returns>
        public List<Vector2I> FindPath(ref Vector2I start, ref Vector2I goal)
        {
            var path = new List<Vector2I>();
            var map = new Node[Width, Height];
            var open = new PriorityLinkedQueue<Node>();

            var startH = HeuristicCostEstimate(start, goal);
            var startNode = new Node(ref start, 0, startH);
            map[start.X, start.Y] = startNode;
            open.Enqueue(startNode);
            var closestToGoal = startNode;

            while (!open.Empty)
            {
                var current = open.Dequeue();
                if (current.Equals(goal))
                {
                    ReconstructPath(path, map, current);
                    return path;
                }

                current.Closed = true;
                var currentPosition = current.Position;
                
                foreach (var neighborReadonly in currentPosition.FourNeighbors())
                {
                    var neighbor = neighborReadonly;
                    if (!IsPassable(ref neighbor)) continue;

                    var neighborNode = map[neighbor.X, neighbor.Y];

                    if (neighborNode == null)
                    {
                        neighborNode = new Node(ref neighbor);
                        map[neighbor.X, neighbor.Y] = neighborNode;
                    }
                    else if (neighborNode.Closed)
                    {
                        continue;
                    }

                    var tentativeG = current.G + 1;
                    var notInOpen = !open.Contains(neighborNode);
                    if (notInOpen || tentativeG < neighborNode.G)
                    {
                        neighborNode.CameFrom = current;
                        neighborNode.G = tentativeG;
                        neighborNode.H = HeuristicCostEstimate(neighbor, goal);
                        neighborNode.F = neighborNode.G + neighborNode.H;
                        if (notInOpen)
                        {
                            open.Enqueue(neighborNode);
                        }

                        if (neighborNode.H < closestToGoal.H ||
                            neighborNode.H.Equals(closestToGoal.H) && neighborNode.G < closestToGoal.G)
                        {
                            closestToGoal = neighborNode;
                        }
                    }
                }
            }

            ReconstructPath(path, map, closestToGoal);
            return path;
        }

        private void ReconstructPath(List<Vector2I> path, Node[,] map, Node current)
        {
            if (current.CameFrom != null)
            {
                ReconstructPath(path, map, current.CameFrom);
            }

            path.Add(current.Position);
        }

        private static float HeuristicCostEstimate(Vector2I start, Vector2I goal)
        {
            return Math.Abs(start.X - goal.X) + Math.Abs(start.Y - goal.Y);
        }
    }
}
