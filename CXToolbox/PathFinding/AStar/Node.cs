﻿using System;
using CXToolbox.Utilities;
using Microsoft.Xna.Framework;

namespace CXToolbox.PathFinding.AStar
{
    public class Node : IComparable<Node>, IEquatable<Node>, IEquatable<Vector2I>
    {
        public Vector2I Position;
        public float F;
        public float G;
        public float H;
        public Node CameFrom;
        public bool Closed;

        public Node(ref Vector2I position, float g, float h)
        {
            Position = position;
            G = g;
            H = h;
            F = g + h;
        }

        public Node(ref Vector2I position)
        {
            Position = position;
        }

        public int CompareTo(Node other)
        {
            var diff = F - other.F;
            return diff < 0 ? -1 : (diff > 0 ? 1 : 0);
        }

        public bool Equals(Node other)
        {
            return Position.Equals(other.Position);
        }

        public bool Equals(Vector2I other)
        {
            return Position.Equals(other);
        }
    }
}
