﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace CXToolbox.Tiled
{
    public class TmxMap
    {
        public List<Layer> Layers;
        public List<Tileset> Tilesets;

        public string Version;             // The TMX format version, generally 1.0.
        public MapOrientation Orientation; // Map orientation. Tiled supports "orthogonal" and "isometric" at the moment.
        public int Width;                  // The map width in tiles.
        public int Height;                 // The map height in tiles.
        public int TileWidth;              // The width of a tile.
        public int TileHeight;             // The height of a tile

        public TmxMap()
        {
            Layers = new List<Layer>();
            Tilesets = new List<Tileset>();
        }

        public static TmxMap Open(TextReader reader)
        {
            var doc = new XmlDocument();
            var map = new TmxMap();

            doc.Load(reader);
            XmlNode rootNode = null;

            foreach (XmlNode node in doc.ChildNodes)
            {
                if (node.Name == "map" && node.HasChildNodes)
                {
                    rootNode = node;
                    break;
                }
            }

            if (rootNode == null)
            {
                throw new Exception("Tried to load a file that does not contain map data.");
            }

            map.Version = rootNode.ReadAttribute("version");
            map.Width = rootNode.ReadIntAttribute("width");
            map.Height = rootNode.ReadIntAttribute("height");
            map.TileWidth = rootNode.ReadIntAttribute("tilewidth");
            map.TileHeight = rootNode.ReadIntAttribute("tileheight");

            switch (rootNode.ReadAttribute("orientation"))
            {
                case "isometric": map.Orientation = MapOrientation.Isometric; break;
                default: map.Orientation = MapOrientation.Orthogonal; break;
            }

            foreach (XmlNode childNode in rootNode.ChildNodes)
            {
                switch (childNode.Name)
                {
                    case "tileset":
                        var tileset = Tileset.Parse(childNode);
                        map.Tilesets.Add(tileset);
                        break;
                    case "layer":
                        var layer = Layer.Parse(childNode);
                        map.Layers.Add(layer);
                        break;
                }
            }

            return map;
        }

        public void ComputeSourceRectangle()
        {
            foreach (var layer in Layers)
            {
                foreach (var tile in layer.Tiles)
                {
                    for (var i = Tilesets.Count - 1; i >= 0; i--)
                    {
                        var tileset = Tilesets[i];
                        var idInTileset = tile.Gid - tileset.FirstGid;
                        if (idInTileset >= 0)
                        {
                            tile.TilesetIndex = i;
                            tile.TilesetX = idInTileset % tileset.NumTilesX * tileset.TileWidth;
                            tile.TilesetY = idInTileset / tileset.NumTilesY * tileset.TileHeight;
                            break;
                        }
                    }
                }
            }
        }
    }

    public enum MapOrientation
    {
        Orthogonal,
        Isometric
    }
}
