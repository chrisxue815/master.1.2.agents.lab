﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace CXToolbox.Tiled
{
    public class Layer
    {
        public string Name;           // The name of the layer.
        public int X;                 // The x coordinate of the layer in tiles. Defaults to 0 and can no longer be changed in Tiled Qt.
        public int Y;                 // The y coordinate of the layer in tiles. Defaults to 0 and can no longer be changed in Tiled Qt.
        public int Width;             // The width of the layer in tiles. Traditionally required, but as of Tiled Qt always the same as the map width.
        public int Height;            // The height of the layer in tiles. Traditionally required, but as of Tiled Qt always the same as the map height.
        public double Opacity;        // The opacity of the layer as a value from 0 to 1. Defaults to 1.
        public bool Visible;          // Whether the layer is shown (1) or hidden (0). Defaults to 1.
        public Dictionary<string, object> Properties;
        public Tile[,] Tiles;

        public Layer(int width, int height)
        {
            Width = width;
            Height = height;
            Properties = new Dictionary<string, object>();
            Tiles = new Tile[Width, Height];
        }

        public static Layer Parse(XmlNode node)
        {
            var width = node.ReadIntAttribute("width");
            var height = node.ReadIntAttribute("height");
            var layer = new Layer(width, height)
            {
                Name = node.ReadAttribute("name"),
                X = node.ReadIntAttribute("x"),
                Y = node.ReadIntAttribute("y"),
                Opacity = node.ReadDoubleAttribute("opacity", 1),
                Visible = node.ReadIntAttribute("visible", 1) == 1
            };

            if (node.HasChildNodes)
            {
                var dataNode = node.FirstChild;
                var data = dataNode.InnerText.Trim('\n', ' ');

                var encoding = dataNode.ReadAttribute("encoding");
                if (encoding != "csv") throw new Exception("TMX must use CSV format");

                data = data.Replace("\n", "");

                var indices = data.Split(',');

                for (var y = 0; y < layer.Height; y++)
                {
                    for (var x = 0; x < layer.Width; x++)
                    {
                        var gid = int.Parse(indices[x + y * layer.Width]);
                        layer.Tiles[x, y] = new Tile(x, y, gid);
                    }
                }
            }

            return layer;
        }
    }
}
