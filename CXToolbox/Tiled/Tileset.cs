﻿using System.Xml;

namespace CXToolbox.Tiled
{
    public class Tileset
    {
        public int FirstGid;
        public string Name;

        public int TileWidth;
        public int TileHeight;

        public string ImageSource;

        public int ImageWidth;
        public int ImageHeight;

        public int NumTilesX;
        public int NumTilesY;

        public static Tileset Parse(XmlNode node)
        {
            var tileset = new Tileset
            {
                FirstGid = node.ReadIntAttribute("firstgid", 1),
                Name = node.ReadAttribute("name"),
                TileWidth = node.ReadIntAttribute("tilewidth"),
                TileHeight = node.ReadIntAttribute("tileheight")
            };

            if (node.HasChildNodes)
            {
                var image = node.FirstChild;
                tileset.ImageSource = image.ReadAttribute("source");
                tileset.ImageWidth = image.ReadIntAttribute("width");
                tileset.ImageHeight = image.ReadIntAttribute("height");
                tileset.NumTilesX = tileset.ImageWidth / tileset.TileWidth;
                tileset.NumTilesY = tileset.ImageHeight / tileset.TileHeight;
            }

            return tileset;
        }
    }
}
