﻿using System.Xml;

namespace CXToolbox.Tiled
{
    public static class XmlExt
    {
        public static string ReadAttribute(this XmlNode node, string tagName, string defaultValue = "")
        {
            if (node.Attributes != null)
            {
                return node.Attributes[tagName] != null ? node.Attributes[tagName].Value : defaultValue;
            }
            else
            {
                return defaultValue;
            }
        }

        public static int ReadIntAttribute(this XmlNode node, string tagName, int defaultValue = 0)
        {
            if (node.Attributes != null)
            {
                return node.Attributes[tagName] != null ? int.Parse(node.Attributes[tagName].Value) : defaultValue;
            }
            else
            {
                return defaultValue;
            }
        }

        public static double ReadDoubleAttribute(this XmlNode node, string tagName, double defaultValue = 1)
        {
            if (node.Attributes != null)
            {
                return node.Attributes[tagName] != null ? double.Parse(node.Attributes[tagName].Value) : defaultValue;
            }
            else
            {
                return defaultValue;
            }
        }
    }
}
