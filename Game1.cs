using System.Collections.Generic;
using FiniteStateMachine.Agents;
using FiniteStateMachine.Components;
using FiniteStateMachine.FSM;
using FiniteStateMachine.Sensing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FiniteStateMachine
{
    public class Game1 : Game
    {
        public static Game1 Instance { get; private set; }

        public new List<Component> Components;

        public EnvironmentManager EnvironmentManager;
        public RegionalSenseManager RegionalSenseManager;
        public CorpseManager CorpseManager;

        public Miner Bob;
        public MinersWife Elsa;
        public Outlaw Jesse;
        public Sheriff Wyatt;
        public Undertaker Hades;

        public GraphicsDeviceManager Graphics;
        public SpriteBatch SpriteBatch;
        public SpriteFont SpriteFont;

        public bool Running = true;
        public float ElapsedSeconds;
        public float TotalSeconds;
        private bool wasPauseKeyDown;

        private const float AIFrameRate = 5;
        private const float AIElapsedTime = 1.0f / AIFrameRate;
        private float timeAccumulator;

        public Game1()
        {
            Instance = this;
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            Graphics.PreferredBackBufferWidth = 1366;
            Graphics.PreferredBackBufferHeight = 768;
            Graphics.ApplyChanges();

            IsMouseVisible = true;

            Components = new List<Component>();

            EnvironmentManager = new EnvironmentManager();
            Components.Add(EnvironmentManager);

            RegionalSenseManager = new RegionalSenseManager();
            Components.Add(RegionalSenseManager);

            CorpseManager = new CorpseManager();
            Components.Add(CorpseManager);
        }

        protected override void Initialize()
        {
            foreach (var component in Components)
            {
                component.Initialize();
            }

            Bob = new Miner();
            Bob.Name = "Bob";

            Elsa = new MinersWife();
            Elsa.Name = "Elsa";

            Bob.WifeId = Elsa.Id;
            Elsa.HusbandId = Bob.Id;

            Jesse = new Outlaw();
            Jesse.Name = "Jesse";

            Wyatt = new Sheriff();
            Wyatt.Name = "Wyatt";

            Hades = new Undertaker();
            Hades.Name = "Hades";

            AgentManager.Initialize();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            SpriteFont = Content.Load<SpriteFont>("Arial");

            foreach (var component in Components)
            {
                component.LoadContent();
            }

            AgentManager.LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Running)
            {
                ElapsedSeconds = (float) gameTime.ElapsedGameTime.TotalSeconds;
                TotalSeconds = (float) gameTime.TotalGameTime.TotalSeconds;
                timeAccumulator += ElapsedSeconds;

                Message.GameTime = gameTime;

                if (timeAccumulator > AIElapsedTime)
                {
                    foreach (var component in Components)
                    {
                        component.Update();
                    }

                    timeAccumulator -= AIElapsedTime;
                    AgentManager.Update();
                }

                Message.SendDelayedMessages();
            }

            var pauseKeyDown = Keyboard.GetState().IsKeyDown(Keys.Space);
            if (pauseKeyDown && !wasPauseKeyDown)
            {
                Running = !Running;
            }
            wasPauseKeyDown = pauseKeyDown;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            Graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

            SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            foreach (var component in Components)
            {
                component.Draw();
            }

            AgentManager.Draw();

            SpriteBatch.End();

            base.Draw(gameTime);
        }

        public void Draw(Texture2D texture, float layerDepth)
        {
            SpriteBatch.Draw(texture, Vector2.Zero, null, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, layerDepth);
        }

        public void Draw(Texture2D texture, ref Vector2 position, ref Rectangle source, float layerDepth)
        {
            SpriteBatch.Draw(texture, position, source, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, layerDepth);
        }
    }
}
