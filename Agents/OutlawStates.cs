﻿using CXToolbox.Utilities;
using FiniteStateMachine.Components;
using FiniteStateMachine.FSM;
using FiniteStateMachine.Models;

namespace FiniteStateMachine.Agents
{
    public partial class Outlaw
    {
        public class RespawnOutlawCamp : State<Outlaw>
        {
            private float rebornRate;

            public override void Enter(Outlaw agent)
            {
                Printer.Print(agent, "Ehhhhhhhh!");
                Printer.Print(agent, "Ahhhhhhhh!");
                agent.TargetLocation = Location.OutlawCamp;
                agent.CurrentPosition = agent.TargetPosition;
            }

            public override void Execute(Outlaw agent)
            {
                rebornRate += 0.01f;
                var reborn = RandomExt.NextBool(rebornRate, -0.6f);
                if (reborn)
                {
                    Printer.Print(agent, "I'm reborn. Be prepared for my revenge!");
                    agent.Alive = true;
                    agent.StateMachine.ChangeState(new LurkInOutlawCamp());
                }
            }
        }

        public class LurkInOutlawCamp : State<Outlaw>
        {
            public override void Enter(Outlaw agent)
            {
                Printer.Print(agent, "Walkin' to the outlaw camp");
                agent.TargetLocation = Location.OutlawCamp;
            }

            public override void Execute(Outlaw agent)
            {
                float[] possibilities = { 0.3f, 0.15f };

                switch (RandomExt.NextInt(possibilities))
                {
                    case 0:
                        agent.StateMachine.ChangeState(new LurkInCemetery());
                        break;
                    case 1:
                        agent.StateMachine.ChangeState(new RobBank());
                        break;
                }
            }

            public override void Exit(Outlaw agent)
            {
                Printer.Print(agent, "Ah'm leaving the outlaw camp");
            }
        }

        public class LurkInCemetery : State<Outlaw>
        {
            public override void Enter(Outlaw agent)
            {
                Printer.Print(agent, "Walkin' to the cemetery");
                agent.TargetLocation = Location.Cemetery;
            }

            public override void Execute(Outlaw agent)
            {
                float[] possibilities = { 0.3f, 0.15f };

                switch (RandomExt.NextInt(possibilities))
                {
                    case 0:
                        agent.StateMachine.ChangeState(new LurkInOutlawCamp());
                        break;
                    case 1:
                        agent.StateMachine.ChangeState(new RobBank());
                        break;
                }
            }

            public override void Exit(Outlaw agent)
            {
                Printer.Print(agent, "Ah'm leaving the cemetery");
            }
        }

        public class RobBank : State<Outlaw>
        {
            private int numTurns;

            public override void Enter(Outlaw agent)
            {
                Printer.Print(agent, "Walkin' to the bank");
                agent.TargetLocation = Location.Bank;
            }

            public override void Execute(Outlaw agent)
            {
                numTurns++;

                var leavingPossibility = 0.02f * numTurns;
                float[] possibilities = { leavingPossibility, leavingPossibility };

                switch (RandomExt.NextInt(possibilities))
                {
                    case 0:
                        agent.StateMachine.ChangeState(new LurkInOutlawCamp());
                        break;
                    case 1:
                        agent.StateMachine.ChangeState(new LurkInCemetery());
                        break;
                }
            }

            public override void Exit(Outlaw agent)
            {
                var moneyRobbed = 2 * numTurns;
                Printer.Print(agent, "Ah'm leaving the bank. Money robbed: " + moneyRobbed);
            }
        }

        public class OutlawGlobalState : State<Outlaw>
        {
            public override bool OnMesssage(Outlaw agent, Telegram telegram)
            {
                switch (telegram.MessageType)
                {
                    case MessageType.Shoot:
                        agent.MoneyCarrying = 0;
                        agent.StateMachine.ChangeState(new RespawnOutlawCamp());
                        agent.TargetPosition = agent.currentPosition;
                        agent.OnTheWay = false;
                        agent.Alive = false;
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}
