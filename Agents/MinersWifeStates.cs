using System;
using FiniteStateMachine.Components;
using FiniteStateMachine.FSM;

namespace FiniteStateMachine.Agents
{
    public partial class MinersWife
    {
        // In this state, the MinersWife agent does random house work
        public class DoHouseWork : State<MinersWife>
        {
            private static readonly Random Rand = new Random();

            public override void Enter(MinersWife agent)
            {
                Printer.Print(agent, "Time to do some more housework!");
            }

            public override void Execute(MinersWife agent)
            {
                switch (Rand.Next(3))
                {
                    case 0:
                        Printer.Print(agent, "Moppin' the floor");
                        break;
                    case 1:
                        Printer.Print(agent, "Washin' the dishes");
                        break;
                    case 2:
                        Printer.Print(agent, "Makin' the bed");
                        break;
                }
            }

            public override void Exit(MinersWife agent)
            {
            }

            public override bool OnMesssage(MinersWife agent, Telegram telegram)
            {
                return false;
            }
        }

        // In this state, the MinersWife agent goes to the loo
        public class VisitBathroom : State<MinersWife>
        {
            public override void Enter(MinersWife agent)
            {
                Printer.Print(agent, "Walkin' to the can. Need to powda mah pretty li'lle nose");
            }

            public override void Execute(MinersWife agent)
            {
                Printer.Print(agent, "Ahhhhhh! Sweet relief!");
                agent.StateMachine.RevertToPreviousState(); // this completes the state blip
            }

            public override void Exit(MinersWife agent)
            {
                Printer.Print(agent, "Leavin' the Jon");
            }

            public override bool OnMesssage(MinersWife agent, Telegram telegram)
            {
                return false;
            }
        }

        // In this state, the MinersWife prepares food
        public class CookStew : State<MinersWife>
        {
            public override void Enter(MinersWife agent)
            {
                if (!agent.Cooking)
                {
                    // MinersWife sends a delayed message to herself to arrive when the food is ready
                    Printer.Print(agent, "Putting the stew in the oven");
                    Message.DispatchMessage(2, agent.Id, agent.Id, MessageType.StewsReady);
                    agent.Cooking = true;
                }
            }

            public override void Execute(MinersWife agent)
            {
                Printer.Print(agent, "Fussin' over food");
            }

            public override void Exit(MinersWife agent)
            {
                Printer.Print(agent, "Puttin' the stew on the table");
            }

            public override bool OnMesssage(MinersWife agent, Telegram telegram)
            {
                switch (telegram.MessageType)
                {
                    case MessageType.HiHoneyImHome:
                        // Ignored here; handled in WifeGlobalState below
                        return false;
                    case MessageType.StewsReady:
                        // Tell Miner that the stew is ready now by sending a message with no delay
                        Printer.PrintMessageData("Message handled by " + agent.Id + " at time ");
                        Printer.Print(agent, "StewReady! Lets eat");
                        Message.DispatchMessage(0, agent.Id, agent.HusbandId, MessageType.StewsReady);
                        agent.Cooking = false;
                        agent.StateMachine.ChangeState(new DoHouseWork());
                        return true;
                    default:
                        return false;
                }
            }
        }

        // If the agent has a global state, then it is executed every Update() cycle
        public class WifeGlobalState : State<MinersWife>
        {
            private static readonly Random Rand = new Random();

            public override void Enter(MinersWife agent)
            {
            }

            public override void Execute(MinersWife agent)
            {
                // There's always a 10% chance of a state blip in which MinersWife goes to the bathroom
                if (Rand.Next(10) == 1 && !agent.StateMachine.IsInState(new VisitBathroom()))
                {
                    agent.StateMachine.ChangeState(new VisitBathroom());
                }
            }

            public override void Exit(MinersWife agent)
            {
            }

            public override bool OnMesssage(MinersWife agent, Telegram telegram)
            {
                switch (telegram.MessageType)
                {
                    case MessageType.HiHoneyImHome:
                        Printer.PrintMessageData("Message handled by " + agent.Id + " at time ");
                        Printer.Print(agent, "Hi honey. Let me make you some of mah fine country stew");
                        agent.StateMachine.ChangeState(new CookStew());
                        return true;
                    case MessageType.StewsReady:
                        return false;
                    default:
                        return false;
                }
            }
        }
    }
}
