using FiniteStateMachine.FSM;
using FiniteStateMachine.Models;

namespace FiniteStateMachine.Agents
{
    // This class implements a Miner agent; the agent creates and maintains its own 
    // StateMachine that it invokes whenever the game asks it to update (triggered
    // by XNA's Update() method)
    public partial class Miner : Agent
    {
        // Here is the StateMachine that the Miner uses to drive the agent's behaviour
        public StateMachine<Miner> StateMachine;

        // The following can be tweaked to change the basic behaviour of the Miner
        public int MaxNuggets = 3;
        public int ThirstLevel = 5;
        public int ComfortLevel = 5;
        public int TirednessThreshold = 5;

        // This is used to keep track of which other agent is our wife
        public int WifeId;

        // It also knows how much gold it's carrying
        public int GoldCarrying;

        // And it knows its bank balance at any point in time
        public int MoneyInBank;

        // The agent's thirst level increases by one for each update
        public int HowThirsty;

        // The agent's fatigue level is changed by the state machine
        public int HowFatigued;

        // The constructor invokes the base class constructor, which then creates 
        // an id for the new agent object and then creates and initalises the agent's
        // StateMachine
        public Miner()
        {
            Title = "Miner";
            targetLocation = Location.Shack;

            StateMachine = new StateMachine<Miner>(this)
            {
                CurrentState = new GoHomeAndSleepTillRested(),
                GlobalState = new MinerGlobalState()
            };
        }

        // This method is invoked by the Game object as a result of XNA updates 
        public override void Update()
        {
            base.Update();
            HowThirsty += 1;
            StateMachine.Update();
        }

        // This method is invoked when the agent receives a message
        public override bool HandleMessage(Telegram telegram)
        {
            return StateMachine.HandleMessage(telegram);
        }

        // This method checks whether the agent's pockets are full or not, depending on the predefined level
        public bool PocketsFull()
        {
            return GoldCarrying >= MaxNuggets;
        }

        // This method checks whether the agent is thirsty or not, depending on the predefined level
        public bool Thirsty()
        {
            return HowThirsty >= ThirstLevel;
        }

        // This method checks whether the agent is fatigued or not, depending on the predefined level
        public bool Fatigued()
        {
            return HowFatigued >= TirednessThreshold;
        }

        // This method checks whether the agent feels rich enough, depending on the predefined level
        public bool Rich()
        {
            return MoneyInBank >= ComfortLevel;
        }
    }
}
