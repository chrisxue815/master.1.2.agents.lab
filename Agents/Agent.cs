using System.Collections.Generic;
using CXToolbox.Utilities;
using FiniteStateMachine.Components;
using FiniteStateMachine.FSM;
using FiniteStateMachine.Models;
using FiniteStateMachine.Sensing;
using FiniteStateMachine.Sensing.Modalities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FiniteStateMachine.Agents
{
    public abstract class Agent : Component, ISensorEntity
    {
        // Every agent has a numerical id that is set when it is created
        public int Id = -1;
        public string Name;
        public string Title;
        public bool Alive = true;

        public const float MaxSightRange = 10;
        public Sensor Sensor;
        public SightModality SightModality;
        public float Orientation { get; private set; }

        public List<Vector2I> Path;
        public bool OnTheWay;

        #region Current/target location/position
        public Location? CurrentLocation;
        protected Vector2I currentPosition;
        public Vector2I CurrentPosition
        {
            get { return currentPosition; }
            set
            {
                currentPosition = value;
                CurrentLocation = Game.EnvironmentManager.GetLocation(ref currentPosition);
                Game.EnvironmentManager.ToScreenPosition(ref currentPosition, out ScreenPosition);
            }
        }

        public Vector2I TargetPosition;
        protected Location targetLocation;
        public Location TargetLocation
        {
            get { return targetLocation; }
            set
            {
                targetLocation = value;
                Game.EnvironmentManager.GetPosition(targetLocation, out TargetPosition);
            }
        }

        public Vector2 ScreenPosition;
        #endregion

        public DialogueManager DialogueManager;

        protected Texture2D texture;
        protected static Rectangle sourceRectangle = new Rectangle(0, 0, 32, 32);

        protected Texture2D shadeTexture;
        protected static Rectangle[] shadeRectangle =
        {
            new Rectangle(0, 0, 32, 32),
            new Rectangle(32, 0, 32, 32),
            new Rectangle(64, 0, 32, 32),
            new Rectangle(96, 0, 32, 32),
            new Rectangle(128, 0, 32, 32),
        };

        protected Agent()
        {
            AgentManager.Add(this);
            Name = "Agent";
            DialogueManager = new DialogueManager(this);

            Sensor = new Sensor(this);
            Game.RegionalSenseManager.AddSensor(Sensor);

            SightModality = new SightModality(MaxSightRange);
        }

        public virtual void HandleSignal(Signal signal)
        {
        }

        public bool DetectModality(Modality modality)
        {
            return modality is SightModality;
        }

        public override void LoadContent()
        {
            Game.EnvironmentManager.GetPassablePosition(targetLocation, out TargetPosition);
            CurrentPosition = TargetPosition;

            texture = Game.Content.Load<Texture2D>("Characters/" + Title);
            shadeTexture = Game.Content.Load<Texture2D>("ShadeAlpha20");

            DialogueManager.LoadContent();
        }

        public override void Update()
        {
            DialogueManager.Update();

            if (!Alive) return;

            OnTheWay = false;
            if (currentPosition != TargetPosition)
            {
                Path = Game.EnvironmentManager.FindPath(ref currentPosition, ref TargetPosition);
                if (Path != null && Path.Count > 1)
                {
                    OnTheWay = true;
                    Orientation = (Path[1] - CurrentPosition).Orientation;
                    CurrentPosition = Path[1];
                }
            }

            var showingMyExistence = new Signal(SightModality, ref currentPosition);
            Game.RegionalSenseManager.AddSignal(showingMyExistence);
        }

        public override void Draw()
        {
            DialogueManager.Draw();

            if (!Alive) return;

            if (Path != null)
            {
                for (var i = 1; i < Path.Count; i++)
                {
                    var pathPosition = Path[i];
                    Vector2 pathScreenPosition;
                    Game.EnvironmentManager.ToScreenPosition(ref pathPosition, out pathScreenPosition);
                    Game.Draw(shadeTexture, ref pathScreenPosition, ref shadeRectangle[Id], 0.9f);
                }
            }

            Game.Draw(texture, ref ScreenPosition, ref sourceRectangle, 0);
        }

        // Any agent must implement these methods
        public abstract bool HandleMessage(Telegram telegram);
    }
}
