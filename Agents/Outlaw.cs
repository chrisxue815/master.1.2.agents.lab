﻿using FiniteStateMachine.FSM;
using FiniteStateMachine.Models;

namespace FiniteStateMachine.Agents
{
    public partial class Outlaw : Agent
    {
        public StateMachine<Outlaw> StateMachine;

        public int MoneyCarrying;

        public Outlaw()
        {
            Title = "Outlaw";
            targetLocation = Location.OutlawCamp;

            StateMachine = new StateMachine<Outlaw>(this)
            {
                CurrentState = new LurkInOutlawCamp(),
                GlobalState = new OutlawGlobalState()
            };
        }

        public override void Update()
        {
            base.Update();
            StateMachine.Update();
        }

        public override bool HandleMessage(Telegram telegram)
        {
            return StateMachine.HandleMessage(telegram);
        }
    }
}
