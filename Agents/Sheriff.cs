﻿using System.Collections.Generic;
using FiniteStateMachine.Components;
using FiniteStateMachine.FSM;
using FiniteStateMachine.Models;
using FiniteStateMachine.Sensing;

namespace FiniteStateMachine.Agents
{
    public partial class Sheriff : Agent
    {
        public StateMachine<Sheriff> StateMachine;

        public int MoneyCarring;

        public Sheriff()
        {
            Title = "Sheriff";
            targetLocation = Location.SheriffsOffice;

            StateMachine = new StateMachine<Sheriff>(this)
            {
                CurrentState = new StartInSheriffsOffice()
            };
        }

        public override void HandleSignal(Signal signal)
        {
            if (signal.Position.Equals(currentPosition)) return;

            var source = AgentManager.FindAgent(ref signal.Position);
            if (source == null) return;

            var outlaw = source as Outlaw;
            if (outlaw == null)
            {
                var output = string.Format("How are you? {0}.", source.Name);
                Printer.Print(this, output);
                Message.DispatchMessage(0, Id, source.Id, MessageType.Greeting);
            }
            else
            {
                var outlaws = new List<Outlaw> { outlaw };
                StateMachine.ChangeState(new ShootOutlaws(outlaws));
                TargetPosition = currentPosition;
            }
        }

        public override void Update()
        {
            base.Update();
            StateMachine.Update();
        }

        public override bool HandleMessage(Telegram telegram)
        {
            return StateMachine.HandleMessage(telegram);
        }
    }
}
