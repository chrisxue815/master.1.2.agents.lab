﻿using CXToolbox.Utilities;
using FiniteStateMachine.Components;
using FiniteStateMachine.FSM;
using FiniteStateMachine.Models;

namespace FiniteStateMachine.Agents
{
    public partial class Undertaker
    {
        public class HoverInUndertakersOffice : State<Undertaker>
        {
            public override void Enter(Undertaker agent)
            {
                Printer.Print(agent, "Hovering in the Undertaker's office");
                agent.TargetLocation = Location.Undertakers;
            }
        }

        public class SearchForCorpse : State<Undertaker>
        {
            public override void Execute(Undertaker agent)
            {
                foreach (var corpse in agent.Game.CorpseManager.All)
                {
                    if (!corpse.Buried)
                    {
                        agent.StateMachine.ChangeState(new DragCorpse(corpse));
                        return;
                    }
                }
                agent.StateMachine.ChangeState(new HoverInUndertakersOffice());
            }
        }

        public class DragCorpse : State<Undertaker>
        {
            private readonly Corpse corpse;

            public DragCorpse(Corpse corpse)
            {
                this.corpse = corpse;
            }

            public override void Enter(Undertaker agent)
            {
                Printer.Print(agent, "Looking for corpse.");
                agent.TargetPosition = corpse.Position + Vector2I.RandomDirection();
            }

            public override void Execute(Undertaker agent)
            {
                if (corpse.Transformed)
                {
                    var output = string.Format("GHOST!!");
                    Printer.Print(agent, output);
                    agent.StateMachine.ChangeState(new SearchForCorpse());
                }
                else
                {
                    var location = agent.Game.EnvironmentManager.GetLocation(ref corpse.Position);
                    if (location == Location.Cemetery)
                    {
                        var output = string.Format("Burying the corpse of {0} on the spot in the cemetery", corpse.Owner.Name);
                        Printer.Print(agent, output);
                        corpse.Buried = true;
                        agent.StateMachine.ChangeState(new SearchForCorpse());
                    }
                    else
                    {
                        var output = string.Format("Dragging off the corpse of {0} from {1}", corpse.Owner.Name, location);
                        Printer.Print(agent, output);
                        agent.Game.CorpseManager.Remove(corpse);
                        agent.StateMachine.ChangeState(new BuryCorpse(corpse));
                    }
                }
            }
        }

        public class BuryCorpse : State<Undertaker>
        {
            private readonly Corpse corpse;

            public BuryCorpse(Corpse corpse)
            {
                this.corpse = corpse;
            }

            public override void Enter(Undertaker agent)
            {
                var output = string.Format("Draging off the corpse of {0} to the cemetery", corpse.Owner.Name);
                Printer.Print(agent, output);
                agent.TargetLocation = Location.Cemetery;
            }

            public override void Execute(Undertaker agent)
            {
                Printer.Print(agent, "Burying corpse");
                agent.Game.CorpseManager.Add(ref agent.currentPosition, corpse);
                agent.StateMachine.ChangeState(new SearchForCorpse());
            }
        }

        public class UndertakerGlobalState : State<Undertaker>
        {
            public override bool OnMesssage(Undertaker agent, Telegram telegram)
            {
                switch (telegram.MessageType)
                {
                    case MessageType.GunFight:
                        agent.StateMachine.ChangeState(new SearchForCorpse());
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}
