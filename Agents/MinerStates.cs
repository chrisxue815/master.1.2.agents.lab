using FiniteStateMachine.Components;
using FiniteStateMachine.FSM;
using FiniteStateMachine.Models;

namespace FiniteStateMachine.Agents
{
    public partial class Miner
    {
        // This class implements the state in which the Miner agent mines for gold
        public class EnterMineAndDigForNugget : State<Miner>
        {
            public override void Enter(Miner agent)
            {
                Printer.Print(agent, "Walkin' to the goldmine");
                agent.TargetLocation = Location.GoldMine;
            }

            public override void Execute(Miner agent)
            {
                agent.GoldCarrying += 1;
                agent.HowFatigued += 1;
                Printer.Print(agent, "Pickin' up a nugget");
                if (agent.PocketsFull())
                {
                    agent.StateMachine.ChangeState(new VisitBankAndDepositGold());
                }
                if (agent.Thirsty())
                {
                    agent.StateMachine.ChangeState(new QuenchThirst());
                }
            }

            public override void Exit(Miner agent)
            {
                Printer.Print(agent, "Ah'm leaving the gold mine with mah pockets full o' sweet gold");
            }

            public override bool OnMesssage(Miner agent, Telegram telegram)
            {
                return false;
            }
        }

        // In this state, the agent goes to the bank and deposits gold
        public class VisitBankAndDepositGold : State<Miner>
        {
            public override void Enter(Miner agent)
            {
                Printer.Print(agent, "Goin' to the bank. Yes siree");
                agent.TargetLocation = Location.Bank;
            }

            public override void Execute(Miner agent)
            {
                agent.MoneyInBank += agent.GoldCarrying;
                agent.GoldCarrying = 0;
                Printer.Print(agent, "Depositing gold. Total savings now: " + agent.MoneyInBank);
                if (agent.Rich())
                {
                    Printer.Print(agent, "WooHoo! Rich enough for now. Back home to mah li'lle lady");
                    agent.StateMachine.ChangeState(new GoHomeAndSleepTillRested());
                }
                else
                {
                    agent.StateMachine.ChangeState(new EnterMineAndDigForNugget());
                }
            }

            public override void Exit(Miner agent)
            {
                Printer.Print(agent, "Leavin' the Bank");
            }

            public override bool OnMesssage(Miner agent, Telegram telegram)
            {
                return false;
            }
        }

        // In this state, the agent goes home and sleeps
        public class GoHomeAndSleepTillRested : State<Miner>
        {
            public override void Enter(Miner agent)
            {
                Printer.Print(agent, "Walkin' Home");
                agent.TargetLocation = Location.Shack;
                Message.DispatchMessage(0, agent.Id, agent.WifeId, MessageType.HiHoneyImHome);
            }

            public override void Execute(Miner agent)
            {
                if (agent.HowFatigued < agent.TirednessThreshold)
                {
                    Printer.Print(agent, "All mah fatigue has drained away. Time to find more gold!");
                    agent.StateMachine.ChangeState(new EnterMineAndDigForNugget());
                }
                else
                {
                    agent.HowFatigued--;
                    Printer.Print(agent, "ZZZZZ....");
                }
            }

            public override void Exit(Miner agent)
            {
            }

            public override bool OnMesssage(Miner agent, Telegram telegram)
            {
                switch (telegram.MessageType)
                {
                    case MessageType.HiHoneyImHome:
                        return false;
                    case MessageType.StewsReady:
                        Printer.PrintMessageData("Message handled by " + agent.Id + " at time ");
                        Printer.Print(agent, "Okay Hun, ahm a comin'!");
                        agent.StateMachine.ChangeState(new EatStew());
                        return true;
                    default:
                        return false;
                }
            }
        }

        // In this state, the agent goes to the saloon to drink
        public class QuenchThirst : State<Miner>
        {
            public override void Enter(Miner agent)
            {
                if (agent.CurrentLocation != Location.Saloon)
                {
                    Printer.Print(agent, "Boy, ah sure is thusty! Walking to the saloon");
                    agent.TargetLocation = Location.Saloon;
                }
            }

            public override void Execute(Miner agent)
            {
                // Buying whiskey costs 2 gold but quenches thirst altogether
                agent.HowThirsty = 0;
                agent.MoneyInBank -= 2;
                Printer.Print(agent, "That's mighty fine sippin' liquer");
                agent.StateMachine.ChangeState(new EnterMineAndDigForNugget());
            }

            public override void Exit(Miner agent)
            {
                Printer.Print(agent, "Leaving the saloon, feelin' good");
            }

            public override bool OnMesssage(Miner agent, Telegram telegram)
            {
                return false;
            }
        }

        // In this state, the agent eats the food that Elsa has prepared
        public class EatStew : State<Miner>
        {
            public override void Enter(Miner agent)
            {
                Printer.Print(agent, "Smells Reaaal goood Elsa!");
            }

            public override void Execute(Miner agent)
            {
                Printer.Print(agent, "Tastes real good too!");
                agent.StateMachine.RevertToPreviousState();
            }

            public override void Exit(Miner agent)
            {
                Printer.Print(agent, "Thankya li'lle lady. Ah better get back to whatever ah wuz doin'");
            }

            public override bool OnMesssage(Miner agent, Telegram telegram)
            {
                return false;
            }
        }

        // If the agent has a global state, then it is executed every Update() cycle
        public class MinerGlobalState : State<Miner>
        {
            public override void Enter(Miner agent)
            {
            }

            public override void Execute(Miner agent)
            {
            }

            public override void Exit(Miner agent)
            {
            }

            public override bool OnMesssage(Miner agent, Telegram telegram)
            {
                return false;
            }
        }
    }
}
