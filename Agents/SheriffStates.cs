﻿using System.Collections.Generic;
using CXToolbox.Utilities;
using FiniteStateMachine.Components;
using FiniteStateMachine.FSM;
using FiniteStateMachine.Models;

namespace FiniteStateMachine.Agents
{
    public partial class Sheriff
    {
        public class StartInSheriffsOffice : State<Sheriff>
        {
            public StartInSheriffsOffice()
            {
            }

            public override void Enter(Sheriff agent)
            {
                Printer.Print(agent, "Ready to work.");
            }

            public override void Execute(Sheriff agent)
            {
                agent.StateMachine.ChangeState(Patrol.PatrolRandomLocation());
            }
        }

        public class Patrol : State<Sheriff>
        {
            private readonly Location location;

            public Patrol(Location location)
            {
                this.location = location;
            }

            public static Patrol PatrolRandomLocation()
            {
                var newLocation = RandomExt.NextEnumExcluding(Location.SheriffsOffice, Location.OutlawCamp);
                return new Patrol(newLocation);
            }

            public Patrol PatrolNewRandomLocation()
            {
                var newLocation = RandomExt.NextEnumExcluding(Location.SheriffsOffice, Location.OutlawCamp, location);
                return new Patrol(newLocation);
            }

            public override void Enter(Sheriff agent)
            {
                Printer.Print(agent, "Going to patrol " + location);
                agent.TargetLocation = location;
            }

            public override void Execute(Sheriff agent)
            {
                agent.StateMachine.ChangeState(PatrolNewRandomLocation());
            }
        }

        public class ShootOutlaws : State<Sheriff>
        {
            private readonly List<Outlaw> outlaws;

            public ShootOutlaws(List<Outlaw> outlaws)
            {
                this.outlaws = outlaws;
            }

            public override void Execute(Sheriff agent)
            {
                var moneyCollected = 0;
                foreach (var outlaw in outlaws)
                {
                    var corpsePosition = outlaw.CurrentPosition;
                    agent.Game.CorpseManager.Add(ref corpsePosition, outlaw);

                    moneyCollected += outlaw.MoneyCarrying;
                    outlaw.MoneyCarrying = 0;
                    Message.DispatchMessage(0, agent.Id, outlaw.Id, MessageType.Shoot);

                    var undertaker = AgentManager.FindAgent<Undertaker>();
                    if (undertaker != null)
                    {
                        Message.DispatchMessage(0, agent.Id, undertaker.Id, MessageType.GunFight);
                    }
                }

                var output = string.Format("Executed {0} outlaw, money collected: {1}", outlaws.Count, moneyCollected);
                Printer.Print(agent, output);
                agent.MoneyCarring += moneyCollected;

                if (agent.MoneyCarring > 0)
                {
                    agent.StateMachine.ChangeState(new ReturnMoneyToBank());
                }
                else
                {
                    agent.StateMachine.ChangeState(Patrol.PatrolRandomLocation());
                }
            }
        }

        public class ReturnMoneyToBank : State<Sheriff>
        {
            public override void Enter(Sheriff agent)
            {
                Printer.Print(agent, "Returning collected money to the bank");
                agent.TargetLocation = Location.Bank;
            }

            public override void Execute(Sheriff agent)
            {
                Printer.Print(agent, "Money returned to bank: " + agent.MoneyCarring);
                agent.MoneyCarring = 0;
                agent.StateMachine.ChangeState(new CelebrateAtSaloon());
            }
        }

        public class CelebrateAtSaloon : State<Sheriff>
        {
            public override void Enter(Sheriff agent)
            {
                Printer.Print(agent, "Going to the saloon");
                agent.TargetLocation = Location.Saloon;
            }

            public override void Execute(Sheriff agent)
            {
                Printer.Print(agent, "Fun time!");
                agent.StateMachine.ChangeState(new StartInSheriffsOffice());
            }
        }
    }
}
