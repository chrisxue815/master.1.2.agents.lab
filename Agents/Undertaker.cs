﻿using System.Collections.Generic;
using FiniteStateMachine.FSM;
using FiniteStateMachine.Models;

namespace FiniteStateMachine.Agents
{
    public partial class Undertaker : Agent
    {
        public StateMachine<Undertaker> StateMachine;

        public Undertaker()
        {
            Title = "Undertaker";
            targetLocation = Location.Undertakers;

            StateMachine = new StateMachine<Undertaker>(this)
            {
                CurrentState = new HoverInUndertakersOffice(),
                GlobalState = new UndertakerGlobalState()
            };
        }

        public override void Update()
        {
            base.Update();
            StateMachine.Update();
        }

        public override bool HandleMessage(Telegram telegram)
        {
            return StateMachine.HandleMessage(telegram);
        }
    }
}
