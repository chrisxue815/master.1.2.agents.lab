using System;
using FiniteStateMachine.FSM;
using FiniteStateMachine.Models;

namespace FiniteStateMachine.Agents
{
    // This class implements a MinersWife agent; the agent creates and maintains its own 
    // StateMachine that it invokes whenever the game asks it to update (triggered
    // by XNA's Update() method)
    public partial class MinersWife : Agent
    {
        public StateMachine<MinersWife> StateMachine;

        // This is used to keep track of which other agent is our husband
        public int HusbandId;

        public Boolean Cooking;

        // The constructor invokes the base class constructor, which then creates 
        // an id for the new agent object and then creates and initalises the agent's
        // StateMachine
        public MinersWife()
        {
            Title = "MinersWife";
            targetLocation = Location.Shack;

            StateMachine = new StateMachine<MinersWife>(this)
            {
                CurrentState = new DoHouseWork(),
                GlobalState = new WifeGlobalState()
            };
        }

        // This method is invoked by the Game object as a result of XNA updates 
        public override void Update()
        {
            base.Update();
            StateMachine.Update();
        }

        // This method is invoked when the agent receives a message
        public override bool HandleMessage(Telegram telegram)
        {
            return StateMachine.HandleMessage(telegram);    
        }
    }
}
