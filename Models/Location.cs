﻿using System;

namespace FiniteStateMachine.Models
{
    // Here is the list of locations that agents can visit
    public enum Location
    {
        Shack,
        GoldMine,
        Bank,
        Saloon,
        OutlawCamp,
        SheriffsOffice,
        Undertakers,
        Cemetery
    }

    public static class LocationExt
    {
        public static Array Values = Enum.GetValues(typeof(Location));
        public static int Count = Values.Length;
    }
}
