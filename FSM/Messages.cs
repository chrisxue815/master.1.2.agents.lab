using System;
using System.Collections.Generic;
using FiniteStateMachine.Agents;
using FiniteStateMachine.Components;
using Microsoft.Xna.Framework;

namespace FiniteStateMachine.FSM
{
    // The types of messages that our agents can send to each other
    public enum MessageType
    {
        HiHoneyImHome,
        StewsReady,
        Greeting,
        Shoot,
        GunFight
    }

    // Telegrams are the messages that are sent between agents -- don't create these yourself, just call DispatchMessage()
    public struct Telegram
    {
        public double DispatchTime;
        public int Sender;
        public int Receiver;
        public MessageType MessageType;

        public Telegram(double dt, int s, int r, MessageType mt)
        {
            DispatchTime = dt;
            Sender = s;
            Receiver = r;
            MessageType = mt;
        }
    }

    // This static class encapsulates all the message-related functions in our game
    public static class Message
    {
        public static List<Telegram> TelegramQueue = new List<Telegram>();
        public static GameTime GameTime;

        // This message is used by agents to dispatch messages to other agents -- use this from your own agents
        public static void DispatchMessage(double delay, int sender, int receiver, MessageType messageType)
        {
            var sendingAgent = AgentManager.FindAgent(sender);
            var receivingAgent = AgentManager.FindAgent(receiver);

            var telegram = new Telegram(0, sender, receiver, messageType);
            if (delay <= 0.0f)
            {
                Printer.PrintMessageData("Instant telegram dispatched by " + sender + " for " + receiver + " message is " + MessageToString(messageType));
                SendMessage(receivingAgent, telegram);
            }
            else
            {
                telegram.DispatchTime = (int)GameTime.TotalGameTime.Ticks + delay;
                TelegramQueue.Add(telegram);
                Printer.PrintMessageData("Delayed telegram from " + sender + " recorded at time " + GameTime.TotalGameTime.Ticks);
            }
        }

        // This sends any messages that are due for delivery; invoked at each tick by the game's Update() method
        public static void SendDelayedMessages()
        {
            for (int i = 0; i < TelegramQueue.Count; i++)
            {
                if (TelegramQueue[i].DispatchTime <= GameTime.TotalGameTime.Ticks)
                {
                    Agent receivingAgent = AgentManager.FindAgent(TelegramQueue[i].Receiver);
                    SendMessage(receivingAgent, TelegramQueue[i]);
                    TelegramQueue.RemoveAt(i);
                }
            }
        }

        // Attempt to send a message to a particular agent; called by the preceding two methods -- don't call this from your own agents
        public static void SendMessage(Agent agent, Telegram telegram)
        {
            if (!agent.HandleMessage(telegram))
            {
                Printer.PrintMessageData("Message not handled");
            }
        }

        // Converts a message to string format
        public static String MessageToString(MessageType messageType)
        {
            switch (messageType)
            {
                case MessageType.HiHoneyImHome:
                    return "Hi Honey I'm Home";
                case MessageType.StewsReady:
                    return "Stew's Ready";
                default:
                    return "Not recognized";
            }
        }
    }
}
