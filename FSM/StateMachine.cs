using System;
using FiniteStateMachine.Agents;

namespace FiniteStateMachine.FSM
{
    // This class implements normal states, global states and state blips for a given agent.
    // The agent should create its own StateMachine when its constructor is called.
    public class StateMachine<T> where T: Agent
    {
        private readonly T owner;

        // This holds the current state for the state machine
        public State<T> CurrentState;

        // The agent's previous state is needed to implement state blips
        public State<T> PreviousState;

        // The agent's global state is always executed, if it exists
        public State<T> GlobalState;

        // What a lovely constructor
        public StateMachine(T owner)
        {
            this.owner = owner;
        }

        // This is called by the Agent whenever the Game invokes the Agent's Update() method
        public void Update()
        {
            if (owner.OnTheWay) return;

            if (GlobalState != null)
            {
                GlobalState.Execute(owner);
            }
            if (CurrentState != null)
            {
                CurrentState.Execute(owner);
            }
        }

        // This method attempts to deliver a message first via the global state, and if that fails
        // via the current state
        public bool HandleMessage(Telegram telegram)
        {
            if (GlobalState != null)
            {
                if (GlobalState.OnMesssage(owner, telegram))
                {
                    return true;
                }

            }
            if (CurrentState != null)
            {
                if (CurrentState.OnMesssage(owner, telegram))
                {
                    return true;
                }
            }
            return false;
        }

        // Switch to a new state and save the old one, so we can revert to it later if it's a state blip
        public void ChangeState(State<T> newState)
        {
            PreviousState = CurrentState;
            CurrentState.Exit(owner);
            CurrentState = newState;
            CurrentState.Enter(owner);
        }

        // Invoked when a state blip is finished
        public void RevertToPreviousState()
        {
            ChangeState(PreviousState);
        }

        // Checks whether the machine is in a given state
        public Boolean IsInState(State<T> state)
        {
            return (state.GetType() == CurrentState.GetType());
        }	
    }
}
